## Meeting Guidelines

Every meeting should have clear:

- Intention. What do we want to achieve?

## Calling a Meeting

- Choose what kind of meeting you would like to call, modifying as necessary.
- Determine who will play each role.

### Meeting Types

#### Scrum

- Check in on a daily basis during a Sprint.

#### Design Meeting

- Reach a collaborative understanding about the intention of the feature.
- Define each designer's responsibilities.

#### Retrospective

Primary questions:

- What did we achieve?
- What challenges did we face?
- What were our greatest successes?
- What could we improve on?
